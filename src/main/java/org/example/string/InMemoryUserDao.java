package org.example.string;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InMemoryUserDao implements UserDao{
    @Override
    public List<String> getUsers() {
        return List.of("Petia","Vasia");
    }
}
