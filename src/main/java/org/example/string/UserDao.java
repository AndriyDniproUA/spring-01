package org.example.string;

import java.util.List;

public interface UserDao {
    List<String> getUsers();
}
